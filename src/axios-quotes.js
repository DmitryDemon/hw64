import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://exam8-dima-k.firebaseio.com/'
});


export default instance