import React, {Component} from 'react';
import AddTaskForm from "./ToDo/AddTaskForm";
import Task from "./ToDo/Task";

class TodoListFilm extends Component {
    state = {
        tasks: [],
        textInput: '',
    };

    changeFilm = (value, id) => {
        const tasks = this.state.tasks ;
        tasks[id].text = value;
        this.setState({tasks});
    };


    addTask = () => {
        if (this.state.textInput !== '') {
            const tasks =  [...this.state.tasks];
            const newTask = {
                text: this.state.textInput,
                id: Date.now()
            };

            tasks.push(newTask);


            this.setState({tasks});
        }
    };


    removeItems = id => {
        const tasks = this.state.tasks;

        tasks.splice(id, 1);

        this.setState({tasks})
    };

    changeHandler = (event) => {
        this.setState({
            textInput: event.target.value,
        })
    };


    render() {
        return (
            <div>
                <h1>To watch list</h1>
                <AddTaskForm change={(event) => this.changeHandler(event)} add={this.addTask}/>
                {this.state.tasks.map((task, id)=> {
                    return <Task key={id}
                                 value={task.text}
                                 id={id}
                                 remove={() => this.removeItems(id)}
                                 changed={(e) => this.changeFilm(e.target.value, id)}
                    />
                })}
            </div>
        );
    }
}

export default TodoListFilm;