import React, {Component} from 'react';
import AddTaskForm from "./ToDo/AddTaskForm";
import Task from "./ToDo/Task";
import axios from "axios";

class TodoListPurchase extends Component {



    state = {
        tasks: [],
        textInput: ''
    };

    componentDidMount(){
        axios.get('https://hw64-dima.firebaseio.com/tasks.json').then(response => {
            const tasks = Object.keys(response.data).map(id => {
                return {...response.data[id], id}
            });
            this.setState({tasks})
        })
    }

    // componentWillUnmount() {
    //
    // }

    addTask = () => {
        if (this.state.textInput !== '') {
            const newTask = {
                text: this.state.textInput,
                id: Date.now()
            };

            this.submitNewTsak(newTask).then(response => {

                const tasks =  [...this.state.tasks, newTask];
                this.setState({
                    tasks: tasks
                });
            })
        }
    };

    removeItems = id => {
        const tasks = [...this.state.tasks];
        const index = tasks.findIndex(task => {
            return (
                task.id === id
            )
        });
        tasks.splice(index, 1);
        this.removeTsak(id).then(() => this.setState({tasks}));

    };

    changeHandler = (event) => {
        this.setState({
            textInput: event.target.value
        })
    };

    submitNewTsak = task => {
        return axios.post('https://hw64-dima.firebaseio.com/tasks.json', task);
    };

    removeTsak = id => {
        return axios.delete(`https://hw64-dima.firebaseio.com/tasks/${id}.json`);
    };


    render() {
        return (
            <div>
                <h1>ToDo List</h1>
                <AddTaskForm change={(event) => this.changeHandler(event)}
                             add={() => this.addTask()}

                />
                <Task entries = {this.state.tasks}
                      remove = {(id) => this.removeItems(id)}
                />

            </div>
        );
    }
}

export default TodoListPurchase;