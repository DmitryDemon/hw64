import React, {Component, Fragment} from 'react';
import {Switch, Route, NavLink as RouterNavLink} from "react-router-dom";
import Menu from "./components/Menu";

import TodoListFilm from "./components/TodoListFilm/TodoListFilm";
import TodoListPurchase from "./components/TodoListPurchase/TodoListPurchase";

import {Collapse, Container, Nav, Navbar, NavbarBrand, NavbarToggler, NavItem, NavLink} from "reactstrap";

import './App.css';

class App extends Component {



    render() {
        return (
                <Fragment>
                    <Navbar dark color='dark' expand="md">
                        <NavbarBrand>TODOшищееее</NavbarBrand>
                        <NavbarToggler/>
                        <Collapse isOpen navbar>
                            <Nav className="ml-auto" navbar>
                                <NavItem>
                                    <NavLink tag={RouterNavLink} to='/' exact>Menu</NavLink>
                                </NavItem>
                                <NavItem>
                                    <NavLink tag={RouterNavLink} to="/todo/film">Todo_Film_list</NavLink>
                                </NavItem>
                                <NavItem>
                                    <NavLink tag={RouterNavLink} to="/todo/purchase">Todo_Purchase_list</NavLink>
                                </NavItem>
                            </Nav>
                        </Collapse>
                    </Navbar>
                    <Container>
                        <Switch>
                            <Route path="/" exact component={Menu}/>
                            <Route path="/todo/film" exact component={TodoListFilm}/>
                            <Route path="/todo/purchase" component={TodoListPurchase}/>
                        </Switch>
                    </Container>
                </Fragment>
        );
    }
}

export default App;
